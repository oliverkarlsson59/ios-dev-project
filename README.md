# iOS-Dev Project
This is a project made for our iOS Development Course where we chose to make an 
n puzzle game. It is being developed by Alfred Runn, Joacim Lilja, Oliver Karlsson and Simon Enström

#Built with

1. [Xcode](https://developer.apple.com/xcode/) - IDE used
2. [Swift](https://developer.apple.com/swift/) - Language used



#Installing

If it crashes when trying to select different levels, try Cleaning build folder and then build it on a different simulator
